import { useState } from 'react'
import './App.scss'

type InputNumber = number | null

let tipsList: number[] = [5, 10, 15, 25, 50]

function App() {
  const [bill, setBill] = useState<InputNumber>(null)
  const [tip, setTip] = useState<InputNumber>(null)
  const [customTip, setCustomTip] = useState<InputNumber>(null)
  const [people, setPeople] = useState<InputNumber>(null)

  function setTipAndClear(fixedTip: number) {
    setCustomTip(null)
    setTip(fixedTip)
  }

  function setCustomTipAndClear(customTip: InputNumber) {
    setTip(null)
    setCustomTip(customTip)
  }

  function resetState() {
    if (bill === 123456) {
      const len = Math.floor(Math.random() * 8) + 4
      tipsList = Array(len).fill(0).map(() => Math.floor(Math.random() * 100))
      // return
    }
    setBill(null)
    setTip(null)
    setCustomTip(null)
    setPeople(null)
  }

  function getTipAmount(): number {
    const finalTip = customTip || tip
    if (!bill || !finalTip || !people) return 0
    return bill * finalTip * 0.01 / people
  }

  function getTotal(): number {
    if (!bill || !people) return 0
    return bill / people + getTipAmount()
  }

  return (
    <div className='App'>
      <img src='./logo.svg' alt='Splitter logo' className='logo' />
      <article className='calc'>
        <div className="inputs">
          <label htmlFor="bill" className='label'>Bill</label>
          <input type="number"
            min={1}
            id='bill'
            name='bill'
            placeholder='0'
            className='bill'
            size={1}
            value={String(bill)}
            onChange={e => (e.target.value && e.target.value !== '0') ? setBill(Number(e.target.value)) : setBill(null)}
          />

          <label htmlFor="tip" className='label label--tip'>Select Tip %</label>
          <div className="tips">

            {tipsList.map((curTip, i) => (
              <TipRadioButton tipAmount={curTip} currentTip={tip} id={i} handler={setTipAndClear} key={i} />
            ))}

            <input
              type="number"
              min={1}
              name='custom-tip'
              className='tip-custom'
              placeholder='Custom'
              value={String(customTip)}
              onChange={e => setCustomTipAndClear(Number(e.target.value))}
            />

          </div>

          <label htmlFor="people" className='label'>Number of People</label>
          <input
            type="number"
            min={1}
            id='people'
            name='people'
            placeholder='0'
            className='people'
            size={1}
            value={String(people)}
            onChange={e => (e.target.value && e.target.value !== '0') ? setPeople(Number(e.target.value)) : setPeople(null)}
          />
        </div>
        <div className="result">
          <div>
            <div className="result-row">
              <div className="result-left">
                <div className="result-title">
                  Tip Amount
                </div>
                <div className="result-under">
                  / person
                </div>
              </div>
              <div className="result-right">
                {'$' + getTipAmount().toFixed(2)}
              </div>
            </div>
            <div className="result-row">
              <div className="result-left">
                <div className="result-title">
                  Total
                </div>
                <div className="result-under">
                  / person
                </div>
              </div>
              <div className="result-right">
                {'$' + getTotal().toFixed(2)}
              </div>
            </div>
          </div>
          <input
            type="button"
            value="reset"
            className='reset'
            disabled={bill == null && tip == null && customTip == null && people == null}
            onClick={resetState}
          />
        </div>
      </article>
    </div>
  )
}

interface Props {
  tipAmount: number,
  currentTip: InputNumber,
  id: number | string,
  handler: Function
}

function TipRadioButton({ tipAmount, currentTip, id, handler }: Props) {
  return (
    <>
      <input
        type="radio"
        name="tip"
        id={'id_' + id}
        value={tipAmount}
        checked={currentTip === tipAmount}
        onChange={e => handler(Number(e.target.value))}
      />
      <label htmlFor={'id_' + id} className='tip-label'>{tipAmount}%</label>
    </>
  )
}

export default App
