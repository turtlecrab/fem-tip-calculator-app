# Frontend Mentor - Tip calculator app

![Design preview for the Tip calculator app coding challenge](./design/desktop-preview.jpg)

## Links

- Live site: <https://fem-tip-calculator-app-ebon.vercel.app/>
- Submission page: <https://www.frontendmentor.io/solutions/tip-calc-with-react-typescript-sass-ByqVZsdNq>

## Notes

Made with React, Typescript, Sass. There are definitely better ways to validate inputs in React, maybe I'll refactor it later.